library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- AGU -> Address Generation Unit
entity agu is
	port(clk: in std_logic;
		  baseAdress: in std_logic_vector (12 downto 0);
		  currentLine: in std_logic_vector(4 downto 0);
		  columnSize: in std_logic_vector (6 downto 0);
		  currentColumn: in std_logic_vector (4 downto 0);
		  channelSize: in std_logic_vector (2 downto 0);
		  currentChannel: in std_logic_vector (1 downto 0);
		  address: out std_logic_vector(12 downto 0));
end entity;  

-- Realizar a conta
-- baseAdress + currentLine * columnSize + currentColumn * channelSize + currentChannel
-- ColumnSize = Numero de Colunas * numero de Canais -> Pre calculado


architecture rtl of agu is
	

	signal base_reg, base_reg_1, base_reg_2 : unsigned (12 downto 0);
	signal cl_reg : unsigned (4 downto 0); 	-- currentline
	signal cms_reg: unsigned (6 downto 0);  	-- columnsize
	signal cc_reg: unsigned (4 downto 0);  	-- currentcolumn
	signal chs_reg: unsigned (2 downto 0);		-- channelsize
	signal cch_reg, cch_reg_1, cch_reg_2: unsigned (1 downto 0);		-- currentchannel
	signal address_reg: unsigned (12 downto 0);
	
	signal mult_line: unsigned(11 downto 0);
	signal mult_column: unsigned(7 downto 0);
	
	signal add_lc: unsigned(11 downto 0);
	
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			base_reg <= unsigned(baseAdress);
			cl_reg <= unsigned(currentLine);
			cms_reg <= unsigned(columnSize);
			cc_reg <= unsigned(currentColumn);
			chs_reg <= unsigned(channelSize);
			cch_reg <= unsigned(currentChannel);
		end if;
	end process;
		
	
	-- realizar calculo
	process(clk)
	begin
		if rising_edge(clk) then
			address_reg <= base_reg_2 + add_lc + cch_reg_2;
			mult_line <= cl_reg * cms_reg;
			mult_column <= cc_reg * chs_reg;
			
			add_lc <= mult_line + mult_column;
			--address_reg <= base_reg_2 + cl_reg * cms_reg + cc_reg * chs_reg + cch_reg_2;
			
			
			address <= std_logic_vector(address_reg);
			
			
			-- Registradores pipeline
			--base_reg_1 <= base_reg + cch_reg;
			base_reg_1 <= base_reg;
			base_reg_2 <= base_reg_1;
			cch_reg_1 <= cch_reg;
			cch_reg_2 <= cch_reg_1;
		end if;
	end process;
	
	--address <= std_logic_vector(address_reg);

end architecture;
