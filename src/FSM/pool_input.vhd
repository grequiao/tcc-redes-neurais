library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- AGU -> Address Generation Unit
entity pool_input is
	port(clk: in std_logic;
		  rst: in std_logic;
		  lineSize: in std_logic_vector (4 downto 0);
		  columnSize: in std_logic_vector (4 downto 0);
		  channelSize: in std_logic_vector (2 downto 0);
		  filterQtd: in std_logic_vector (1 downto 0);
		  windowHeight: in std_logic_vector (1 downto 0);
		  windowWidth: in std_logic_vector (1 downto 0);
		  cLine: out std_logic_vector(4 downto 0);
		  cCol: out std_logic_vector(4 downto 0);
		  cChan: out std_logic_vector(1 downto 0);
		  done: out std_logic;
		  newFilter: out std_logic);
end entity;  


architecture rtl of pool_input is
	
	signal currentLine, currentColumn: std_logic_vector(4 downto 0) := "00000";
	signal currentChannel: std_logic_vector(1 downto 0) := "00";
	
	
		
	signal size_channel: integer range 0 to 4 := 0;
	signal size_column: integer range 0 to 32 := 0;
	signal size_line: integer range 0 to 32 := 0;
		
	signal size_x: integer range 0 to 4 := 0;
	signal size_y: integer range 0 to 4 := 0;
	 
	
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			size_channel <= to_integer(unsigned(channelSize));
			size_column <= to_integer(unsigned(columnSize));
			size_line <= to_integer(unsigned(lineSize));
						
			size_x <= to_integer(unsigned(windowWidth));
			size_y <= to_integer(unsigned(windowHeight));
			--filterSize
		end if;
	end process;

	-- Calcular o endereço 
	
	
	process(clk)
		variable channel_counter: integer range 0 to 4 := 0;
		variable column_counter: integer range 0 to 32 := 0;
		variable line_counter: integer range 0 to 32 := 0;
		variable filter_qtd_counter: integer range 0 to 4 := 0;
		
		variable w_x: integer range 0 to 4 := 0;
		variable w_y: integer range 0 to 4 := 0;
		
		variable current_x: integer range 0 to 32 := 0;
		variable current_y: integer range 0 to 32 := 0;
		
		variable finished_filter: std_logic;
		
		variable nFilter: std_logic := '0';
		variable finished: std_logic := '0';
		
	begin
		if rising_edge(clk) then
			nFilter := '0';
			if rst = '1' then
				channel_counter := 0;
				column_counter := 0;
				line_counter := 0;
				filter_qtd_counter := 0;
				w_x := 0;
				w_y := 0;
				current_x := 0;
				current_y := 0;
				finished := '0';
			else
				column_counter := column_counter + 1;
				if w_x = size_x then
					w_x := 0;
					column_counter := current_x;
					line_counter := line_counter + 1;
					if w_y = size_y then
						--line_counter := current_y;
						w_y := 0;
						finished_filter := '1';
					else
						w_y := w_y + 1;
					end if;
				else
					w_x := w_x + 1;
				end if;
				
				if finished_filter = '1' then
					finished_filter := '0';
					nFilter := '1';
					if channel_counter = size_channel then
						channel_counter := 0;
						if current_x = size_column then
							current_x := 0;
							if current_y = size_line then
								current_y := 0;
								finished := '1';
							else
								current_y := current_y + 2;
							end if;
						else
							current_x := current_x + 2;
						end if;
					else
						channel_counter := channel_counter + 1;
					end if;
					column_counter := current_x;
					line_counter := current_y;
				end if;
			end if;
				
			
			currentChannel <= std_logic_vector(to_unsigned(channel_counter, currentChannel'length));
			currentColumn <= std_logic_vector(to_unsigned(column_counter, currentColumn'length));
			currentLine <= std_logic_vector(to_unsigned(line_counter, currentLine'length));
			
			done <= finished;
			newFilter <= nFilter;
		end if;
	end process;
		
	--currentChannel <= std_logic_vector(to_unsigned(channel_counter, currentChannel'length));
	--currentColumn <= std_logic_vector(to_unsigned(column_counter, currentColumn'length));
	--currentLine <= std_logic_vector(to_unsigned(line_counter, currentLine'length));
	--address <= currentLine;

	cLine <= currentLine;
	cCol <= currentColumn;
	cChan <= currentChannel;
	
end architecture;
