library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pool_input_tb is
end entity;  

architecture rtl of pool_input_tb is

	component pool_input is
		port(clk: in std_logic;
			  rst: in std_logic;
			  lineSize: in std_logic_vector (4 downto 0);
			  columnSize: in std_logic_vector (4 downto 0);
			  channelSize: in std_logic_vector (2 downto 0);
			  filterQtd: in std_logic_vector (1 downto 0);
			  windowHeight: in std_logic_vector (1 downto 0);
			  windowWidth: in std_logic_vector (1 downto 0);
			  cLine: out std_logic_vector(4 downto 0);
			  cCol: out std_logic_vector(4 downto 0);
			  cChan: out std_logic_vector(1 downto 0);
			  done: out std_logic;
			  newFilter: out std_logic);
	end component;  	

	
	signal clk: std_logic;	
	signal rst: std_logic;
	signal lineSize, columnSize: std_logic_vector (4 downto 0);
	signal channelSize: std_logic_vector (2 downto 0);
	signal filterQtd, windowHeight, windowWidth: std_logic_vector (1 downto 0);
	
	
	signal cLine, cCol: std_logic_vector (4 downto 0);
	signal cChan: std_logic_vector (1 downto 0);
	signal done, newFilter: std_logic;

	
begin										
	uut : pool_input port map(clk => clk,
										rst => rst,
										lineSize => lineSize,
										columnSize => columnSize,
										channelSize => channelSize,
										filterQtd => filterQtd,
										windowHeight => windowHeight,
										windowWidth => windowWidth,
										cLine => cLine,
										cCol => cCol,
										cChan => cChan,
										done => done,
										newFilter => newFilter);
												
	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	
	process
	begin
		rst <= '1';
		lineSize <= "10000";
		columnSize <= "10000";
		channelSize <= "011";
		filterQtd <= "11";
		windowHeight <= "01";
		windowWidth <= "01";
		wait for 50 ns;
		rst <= '0';
		wait;
	end process;
		
end architecture;
