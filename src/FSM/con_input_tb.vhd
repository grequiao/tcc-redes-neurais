library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity con_input_tb is
end entity;  

architecture rtl of con_input_tb is

	component activation_lut_8bit_impl
		port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0)
		);
	end component;
	
	component agu_weights is
		port(clk: in std_logic;
		  baseAdress: in std_logic_vector (9 downto 0);
		  currentFilter: in std_logic_vector(2 downto 0);
		  kernelSize: in std_logic_vector(6 downto 0);
		  positionKernel: in std_logic_vector(6 downto 0);
		  address: out std_logic_vector(9 downto 0));
	end component;

	component FSM_Input is
		port(clk: in std_logic;
			  rst: in std_logic;
			  lineSize: in std_logic_vector (4 downto 0);
			  columnSize: in std_logic_vector (4 downto 0);
			  channelSize: in std_logic_vector (2 downto 0);
			  filterQtd: in std_logic_vector (1 downto 0);
			  windowHeight: in std_logic_vector (1 downto 0);
			  windowWidth: in std_logic_vector (1 downto 0);
			  strideHeight: in std_logic_vector (1 downto 0);
			  strideWidth: in std_logic_vector (1 downto 0);
			  cLine: out std_logic_vector(4 downto 0);
			  cCol: out std_logic_vector(4 downto 0);
			  cChan: out std_logic_vector(1 downto 0);
			  cFilter: out std_logic_vector(1 downto 0);
			  done: out std_logic;
			  newPool: out std_logic;
			  newFilter: out std_logic);
	end component;  	

	
	signal clk: std_logic;	
	signal rst: std_logic;
	signal lineSize, columnSize: std_logic_vector (4 downto 0);
	signal channelSize: std_logic_vector (2 downto 0);
	signal filterQtd, windowHeight, windowWidth: std_logic_vector (1 downto 0);
	signal strideHeight, strideWidth: std_logic_vector (1 downto 0);
	
	signal cLine, cCol, address: std_logic_vector (4 downto 0);
	signal cChan, cFilter: std_logic_vector (1 downto 0);
	signal done, newPool, newFilter: std_logic;

	
begin										
	uut : FSM_Input port map(clk => clk,
										rst => rst,
										lineSize => lineSize,
										columnSize => columnSize,
										channelSize => channelSize,
										filterQtd => filterQtd,
										windowHeight => windowHeight,
										windowWidth => windowWidth,
										strideHeight => strideHeight,
										strideWidth => strideWidth,
										cLine => cLine,
										cCol => cCol,
										cChan => cChan,
										cFilter => cFilter,
										done => done,
										newPool => newPool,
										newFilter => newFilter);
												
	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	
	process
	begin
		rst <= '1';
		lineSize <= "10000";
		columnSize <= "10000";
		channelSize <= "011";
		filterQtd <= "00";
		windowHeight <= "01";
		windowWidth <= "01";
		strideHeight <= "10";
		strideWidth <= "10";
		wait for 50 ns;
		rst <= '0';
		wait;
	end process;
		
end architecture;
