library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity interpolation_v2_8bits_tb is
end entity;  

architecture rtl of interpolation_v2_8bits_tb is
	component interporlation_v2_8bits is
		port(clk: in std_logic;
		  a: in signed (7 downto 0);
		  b: in signed (7 downto 0);
		  alpha: in unsigned(15 downto 0);
		  output: out signed(24 downto 0));
	end component;  
	
	signal clk: std_logic;
	signal a, b: signed(7 downto 0);
	signal alpha: unsigned(15 downto 0);
	signal output: signed(24 downto 0);
	
begin
	uut : interporlation_v2_8bits port map(clk => clk,
											a => a,
											b => b,
											alpha => alpha,
											output => output);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		a <= x"32";
		b <= x"64";
		alpha <= x"7FFF";
		wait for 100 ns;
		a <= x"64";
		b <= x"C8";
		wait for 100 ns;
		b <= x"A0";
		wait for 100 ns;
		a <= x"FF";
		b <= x"00";
		wait for 100 ns;
		a <= x"FE";
		b <= x"FC";
		wait for 100 ns;
		alpha <= x"FFFF";
		wait for 100 ns;
		a <= x"80";
		wait for 100 ns;
		a <= x"32";
		b <= x"64";
		alpha <= x"7FF0";
		wait;
	end process;
end architecture;
