library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity address_calculator_test is
	port(clk: in std_logic;
		  ibase: in std_logic_vector (12 downto 0);
		  offset: in std_logic_vector (5 downto 0);
		  iWidth: in std_logic_vector(5 downto 0);
		  iLine: in std_logic_vector(5 downto 0);
		  address: out std_logic_vector(12 downto 0));
end entity;  

-- Realizar a conta
-- (ibase + offset) + (iWidth * iLine)

architecture rtl of address_calculator_test is
	component address_mac is
		port (
			result  : out std_logic_vector(12 downto 0); -- result
			dataa_0 : in  std_logic_vector(12 downto 0); -- dataa_0
			dataa_1 : in  std_logic_vector(12 downto 0); -- dataa_1
			datab_0 : in  std_logic_vector(12 downto 0); -- datab_0
			datab_1 : in  std_logic_vector(12 downto 0); -- datab_1
			clock0  : in  std_logic;             			-- clock0
			datac_0 : in  std_logic_vector(12 downto 0); -- datac_0
			datac_1 : in  std_logic_vector(12 downto 0)  -- datac_1
		);
	end component address_mac;
	
	
	signal sBase, sOffset, sWidth, sLine: std_logic_vector (12 downto 0);
	signal address_reg: std_logic_vector (12 downto 0);
	
	constant zero: std_logic_vector(12 downto 0) := "0000000000000"; 
	constant one: std_logic_vector(12 downto 0) := "0000000000001"; 
	
	-- (ibase + offset) * 1 + (iWidth + 0) * iLine
begin

	-- padding
	sOffset <= o"00" & '0' & offset;
	sWidth <= o"00" & '0' & iWidth;
	sLine <= o"00" & '0' & iLine;


	address_mac_inst : component address_mac
		port map (
			result  => address,  --  result.result
			dataa_0 => sWidth, -- dataa_0.dataa_0
			dataa_1 => sBase, -- dataa_1.dataa_1
			datab_0 => zero, -- datab_0.datab_0
			datab_1 => sOffset, -- datab_1.datab_1
			clock0  => clk,  --  clock0.clock0
			datac_0 => sLine, -- datac_0.datac_0
			datac_1 => one  -- datac_1.datac_1
		);
	
	--address <= std_logic_vector(address_reg);

end architecture;
