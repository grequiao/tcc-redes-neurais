library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity relu_unit_tb is
end entity;  

architecture rtl of relu_unit_tb is

	component relu_unit is
		port(clk: in std_logic;
			  rd_en: in std_logic;
			  input: in signed(31 downto 0);
			  zero_point: in signed(31 downto 0);
			  output: out signed (31 downto 0));
	end component;  
	
	signal clk, rd_en: std_logic;
	signal input, zero_point, output: signed(31 downto 0);
	
begin
	uut : relu_unit port map(clk => clk,
									 rd_en => rd_en,
									 input => input,
									 zero_point => zero_point,
									 output => output);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		rd_en <= '1';
		zero_point <= to_signed(255, 32);
		input <= to_signed(511, 32);
		wait for 20 ns;
		rd_en <= '0';
		zero_point <= to_signed(1024, 32);
		input <= to_signed(213, 32);
		wait for 20 ns;
		input <= to_signed(432, 32);
		wait for 20 ns;
		input <= to_signed(50, 32);
		wait for 20 ns;
		input <= to_signed(9999, 32);
		wait for 20 ns;
		input <= to_signed(-99, 32);
		wait for 20 ns;
		input <= to_signed(-500, 32);
		wait for 20 ns;
		zero_point <= to_signed(-10, 32);
		rd_en <= '1';
		input <= to_signed(0, 32);
		wait for 20 ns;
		rd_en <= '0';
		input <= to_signed(1, 32);
		wait for 20 ns;
		input <= to_signed(-123, 32);
		wait for 20 ns;
		input <= to_signed(-999, 32);
		wait for 20 ns;
		input <= to_signed(12, 32);
		wait for 20 ns;
		input <= to_signed(589, 32);
		wait;
	end process;
	
end architecture;
