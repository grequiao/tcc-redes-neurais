library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity converter_v2 is
	port(clk: in std_logic;
		  convert: in signed (31 downto 0);
		  offset: in signed (31 downto 0);
		  multiplier: in signed(31 downto 0);
		  shiftamount: in std_logic_vector (4 downto 0);
		  rd_en: std_logic;
		  output: out signed(31 downto 0));
end entity;  

-- Realiza (X - Z) * S * 2^-M

architecture rtl of converter_v2 is

	component arith_shifter_barrel is
	  port (
			clk					: in  std_logic;
			Input					: in	signed(31 downto 0);
			ShiftAmount			: in	std_logic_vector(4 downto 0);
			Output				: out	signed(31 downto 0)
		);
	end component;
	
--	component converter_mult
--	PORT
--	(
--		clock		: IN STD_LOGIC ;
--		dataa		: IN STD_LOGIC_VECTOR (32 DOWNTO 0);
--		datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
--		result	: OUT STD_LOGIC_VECTOR (64 DOWNTO 0)
--	);
--end component;
	
	signal input_reg, offset_reg, multiplier_reg : signed (31 downto 0);
	signal offset_op : signed (32 downto 0);
	signal offset_op_2: signed(32 downto 0);
	signal multiplier_reg_logic: std_logic_vector(31 downto 0);
	signal mult_op, mult_op2, mult_op3, mult_op4 : signed(33 downto 0);
	signal mult_op_result : signed(31 downto 0);
	signal shift_op: signed(31 downto 0);
	
	signal mult_intermediate: signed(64 downto 0);
	signal mult_intermediate_logic: std_logic_vector(64 downto 0);
	
	signal shiftamount_reg : std_logic_vector (4 downto 0);
	signal mul_reg_pipe : signed (31 downto 0);
	
	constant max_value: signed(33 downto 0) := "0001111111111111111111111111111111";
	constant min_value: signed(33 downto 0) := "1110000000000000000000000000000000";
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			input_reg <= convert;
			if rd_en = '1' then
				shiftamount_reg <= shiftamount;
				offset_reg <= offset;
				multiplier_reg <= multiplier;		
			end if;
		end if;
	end process;
	

	-- operacao
	process(clk)
	begin
		if rising_edge(clk) then
			offset_op <= resize(input_reg, 33) + resize(offset_reg, 33);				-- registrador da LUT/LAB
			--offset_op_2 <= std_logic_vector(offset_op);									-- registrador da entrada do DSP
			offset_op_2 <= offset_op;
		end if;
	end process;
	
	mult_intermediate <= offset_op_2 * multiplier_reg;
	--mult_intermediate <= offset_op * multiplier_reg;
	
	--multiplier_reg_logic <= std_logic_vector(multiplier_reg);	
	--mult : converter_mult port map(clock => clk,
	--										dataa => offset_op_2,
	--										datab => std_logic_vector(multiplier_reg),
	--										signed(result) => mult_intermediate);
											
	--mult_intermediate <= signed(mult_intermediate_logic);
	
	process(clk)
		variable multMem: signed(33 downto 0);
	begin
		if rising_edge(clk) then
			--multMem := mult_intermediate(64 downto 31);
			multMem := mult_op3;
			if multMem > max_value then
				multMem := max_value;
			elsif multMem < min_value then
				multMem := min_value;
			end if;
			
			mult_op <= mult_intermediate(64 downto 31);
			mult_op2 <= mult_op;
			mult_op3 <= mult_op2;
			--mult_op4 <= mult_op3;
			mult_op_result <= multMem(31 downto 0);
		end if;
	end process;
	
	
	shifter : arith_shifter_barrel port map(clk => clk,
														Input => mult_op_result,
														ShiftAmount => shiftamount_reg,
														output => shift_op);
	
	output <= shift_op;


end architecture;
