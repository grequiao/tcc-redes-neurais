library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity converter_tb is
end entity;  

architecture rtl of converter_tb is

	component converter_v2
	port(clk: in std_logic;
		  convert: in signed (31 downto 0);
		  offset: in signed (31 downto 0);
		  multiplier: in signed(31 downto 0);
		  shiftamount: in std_logic_vector (4 downto 0);
		  wr_en: std_logic;
		  output: out signed(31 downto 0));
	end component;
	
	signal clk, wr_en: std_logic;
	signal convert, offset, multiplier: signed(31 downto 0);
	signal shiftamount: std_logic_vector (4 downto 0);
	signal output: signed(31 downto 0);
	
	signal counter: integer range 0 to 1024 := 0;
	
begin
	uut : converter_v2 port map(clk => clk,
										convert => convert,
										offset => offset,
										multiplier => multiplier,
										shiftamount => shiftamount,
										wr_en => wr_en,
										output => output);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		convert <= x"00000000";
		multiplier <= x"6A171025";
		shiftamount <= "00010";
		offset <= x"FFFFFFCE";
		wr_en <= '1';
		wait for 20 ns;
		multiplier <= x"00000000";
		shiftamount <= "00010";
		offset <= x"00000000";
		wr_en <= '0';
		wait for 20 ns;
		convert <= x"00000064"; -- 100
		wait for 20 ns;
		convert <= x"00000096"; -- 150
		wait for 20 ns;
		convert <= x"000000F4"; -- 244
		wait for 20 ns;
		convert <= x"00000010"; -- 16
		wait for 20 ns;
		convert <= x"00000005"; -- 5
		wait for 20 ns;
		convert <= x"000001F4"; -- 500
		wait;
	end process;
	
end architecture;
