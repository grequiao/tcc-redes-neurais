library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity agu_tb is
end entity;  

architecture rtl of agu_tb is

	component activation_lut_8bit_impl
		port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0)
		);
	end component;
	
	component agu is
		port(clk: in std_logic;
			  baseAdress: in std_logic_vector (12 downto 0);
			  currentLine: in std_logic_vector(4 downto 0);
			  columnSize: in std_logic_vector (6 downto 0);
			  currentColumn: in std_logic_vector (4 downto 0);
			  channelSize: in std_logic_vector (2 downto 0);
			  currentChannel: in std_logic_vector (1 downto 0);
			  address: out std_logic_vector(12 downto 0));
	end component;  

	
	signal clk: std_logic;
	signal baseAdress, address: std_logic_vector(12 downto 0);
	signal columnSize: std_logic_vector(6 downto 0);
	signal currentLine, currentColumn: std_logic_vector(4 downto 0);
	signal channelSize: std_logic_vector(2 downto 0);
	signal currentChannel: std_logic_vector(1 downto 0);
	
	--signal test: unsigned(7 downto 0);
	
	signal counter: integer range 0 to 1024 := 0;
	
	signal channel_counter: integer range 0 to 4 := 0;
	signal column_counter: integer range 0 to 32 := 0;
	signal line_counter: integer range 0 to 32 := 0;
	
begin										
	uut : agu port map(clk => clk,
								baseAdress => baseAdress,
								currentLine => currentLine,
								columnSize => columnSize,
								currentColumn => currentColumn,
								channelSize => channelSize,
								currentChannel => currentChannel,
								address => address);
										
	process
	begin
		clk <= '0';
		
		if channel_counter /= 3 then
		--if counter /= 1023 then
			channel_counter <= channel_counter + 1;
		else
			channel_counter <= 0;
			column_counter <= column_counter + 1;
			if column_counter = 27 then
				column_counter <= 0;
				line_counter <= line_counter + 1;
				if line_counter = 27 then
					line_counter <= 0;
				end if;
			end if;
				
		end if;
		
		--if column_counter = 27 and channel_counter = 3 then
		--	column_counter <= 0;
		--	line_counter <= line_counter + 1;
		--end if;
		
		--if line_counter = 27 and column_counter = 27 and channel_counter = 3 then
		--	line_counter <= 0;
		--end if;
		
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	
	process
	begin
		baseAdress <= "0000000000000";
		columnSize  <= "1110000";
		channelSize <= "100";
		wait;
	end process;
	
	currentChannel <= std_logic_vector(to_unsigned(channel_counter, currentChannel'length));
	currentColumn <= std_logic_vector(to_unsigned(column_counter, currentColumn'length));
	currentLine <= std_logic_vector(to_unsigned(line_counter, currentLine'length));
	
end architecture;
