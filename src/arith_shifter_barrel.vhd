library IEEE;
use			IEEE.STD_LOGIC_1164.all;
use			IEEE.NUMERIC_STD.all;


entity arith_shifter_barrel is
  port (
		clk					: in  std_logic;
		Input					: in	signed(31 downto 0);
		ShiftAmount			: in	std_logic_vector(4 downto 0);
		Output				: out	signed(31 downto 0)
	);
end entity;


architecture rtl of arith_shifter_barrel is
	constant STAGES		: positive		:= 5;

	subtype	T_INTERMEDIATE_RESULT is std_logic_vector(31 downto 0);
	type		T_INTERMEDIATE_VECTOR is array (natural range <>) of T_INTERMEDIATE_RESULT;

	signal IntermediateResults	: T_INTERMEDIATE_VECTOR(2 downto 0);
	signal IntermediateResults_pipeline	: T_INTERMEDIATE_VECTOR(3 downto 0);
	signal input_reg, output_reg, pipeline_reg : std_logic_vector(31 downto 0);
	signal ShiftAmount_reg, ShiftAmount_reg_pipe : std_logic_vector(4 downto 0);
begin
	
	process(clk)
	begin
		if rising_edge(clk) then
			input_reg <= std_logic_vector(Input);
			output_reg <= IntermediateResults_pipeline(3);
			ShiftAmount_reg <= ShiftAmount;
		end if;
	end process;

	IntermediateResults(0)	<= input_reg;
	Output	<= signed(output_reg);
	
	
	-- Adicionar pipeline para aumentar a frequencia
	genStage : for i in 0 to 1 generate
		IntermediateResults(i + 1) <= IntermediateResults(i) when ShiftAmount_reg(i) = '0' else
												((2**i - 1) downto 0 => IntermediateResults(i)(31)) & IntermediateResults(i)(31 downto 2**i);
	end generate;
	
		process(clk)
	begin
		if rising_edge(clk) then
			pipeline_reg <= IntermediateResults(2);
			ShiftAmount_reg_pipe <= ShiftAmount_reg;
		end if;
	end process;
	
	IntermediateResults_pipeline(0) <= pipeline_reg;
	
	genStage_2 : for i in 2 to 4 generate
		IntermediateResults_pipeline(i-1) <= IntermediateResults_pipeline(i-2) when ShiftAmount_reg_pipe(i) = '0' else
												((2**i - 1) downto 0 => IntermediateResults_pipeline(i-2)(31)) & IntermediateResults_pipeline(i-2)(31 downto 2**i);
	end generate;
	
end;