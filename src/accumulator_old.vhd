library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity accumulator_old is
	port(clk: in std_logic;
		  load: in std_logic;
		  accumulate: in std_logic;
		  input: in signed (18 downto 0);
		  data: in signed(31 downto 0);
		  output: out signed(33 downto 0));
end entity;  

architecture rtl of accumulator_old is
	
	signal acc: signed (33 downto 0);
	signal resized_data, resized_input: signed (33 downto 0);
	signal op1, op2: signed (33 downto 0);
	
begin

	resized_data <= resize(data, 34);
	resized_input <= resize(input, 34);
	
	op1 <= resized_data when load = '1' else
			 acc;
	
	op2 <= resized_input when accumulate = '1' else
				(others=>'0');

	process(clk)
	begin
		if rising_edge(clk) then
			acc <= op1 + op2;
		end if;
	end process;
	
	output <= acc;

end architecture;
