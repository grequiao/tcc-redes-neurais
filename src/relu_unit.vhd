library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Realizar max(x, zp)
-- Assumindo que Zero Point -> zp

entity relu_unit is
	port(clk: in std_logic;
		  rd_en: in std_logic;
		  input: in signed(31 downto 0);
		  zero_point: in signed(31 downto 0);
		  output: out signed (31 downto 0));
end entity;  

architecture rtl of relu_unit is
	
	signal relu_reg: signed(31 downto 0) := (others => '0');
	signal input_reg_1, input_reg_2: signed(31 downto 0) := (others => '0');
	signal zp_reg: signed(31 downto 0) := (others => '0');
	
	signal gte: std_logic;
	
	--teste
	signal intermediary: signed(31 downto 0) := (others => '0');
	signal input_reg_3: signed(31 downto 0) := (others => '0');
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			input_reg_1 <= input;
			if rd_en = '1' then
				zp_reg <= -zero_point;
			end if;
		end if;
	end process;
		
	-- realizar max(input, zq)
	process(clk)
	begin
		if rising_edge(clk) then
			if input_reg_1 >= zp_reg then
				gte <= '1';
			else
				gte <= '0';
			end if;
						
			if gte = '1' then
				relu_reg <= input_reg_2;
			else
				relu_reg <= zp_reg;
			end if;
			
			input_reg_2 <= input_reg_1;
			
			
			
			--input_reg_3 <= input_reg_2;
			
			--intermediary <= input_reg_1 - zp_reg;
			--if intermediary >= 0 then
			--	gte <= '1';
			--else
			--	gte <= '0';
			--end if;
			
		end if;
	end process;
	
	
	output <= relu_reg;

end architecture;
