library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pool_unit_tb is
end entity;  

architecture rtl of pool_unit_tb is
	component pool_unit is
		port(clk: in std_logic;
			  rst: in std_logic;
			  en: in std_logic;
			  channels: in std_logic_vector (63 downto 0);
			  output: out std_logic_vector (63 downto 0));
	end component; 
	
	
	signal clk, rst, en: std_logic;
	signal a, b : std_logic_vector (63 downto 0);
	
begin
	uut : pool_unit port map(clk => clk,
										rst => rst,
										en => en,
										channels => a,
										output => b);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		rst <= '1';
		en <= '0';
		wait for 100 ns;
		rst <= '0';
		en <= '1';
		wait;
	end process;

	process
	begin
		a <= "0000000000000000000000000000000000000000000000000000000000000000";
		wait for 100 ns;
		a <= "0000111100000000000000010000001000000011100000011000100011000010";
		wait for 20 ns;
		a <= "0001001001110100111001000000101001110101110010010010011001110100";
		wait for 20 ns;
		a <= "1011000011011110101011001001000001000101011111000011011000001010";
		wait for 20 ns;
		a <= "1100010100000110011010001101011100010011000110001111010000011111";
		wait for 20 ns;
		a <= "1010111101010100001001100000000011000110000111010101000000000000";
		wait for 20 ns;
		a <= "1111101111111001011111110100000100100000111111111100100111111010";
	end process;
end architecture;
