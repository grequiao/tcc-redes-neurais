library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity convolution is
	port(clk: in std_logic;
		  rd_en: in std_logic_vector (7 downto 0);
		  activation: in std_logic;
		  load: in std_logic;
		  accumulate: in std_logic;
		  channels: in std_logic_vector (63 downto 0);
		  weight_address: in std_logic_vector (9 downto 0);
		  parameters_adress: in std_logic_vector (7 downto 0);
		  output: out std_logic_vector (63 downto 0));
end entity;  

architecture rtl of convolution is
	component convolution_module_v2 is
		port(clk: in std_logic;
			  rd_en: in std_logic;
			  activation: in std_logic;
			  load: in std_logic;
			  accumulate: in std_logic;
			  channels: in std_logic_vector (63 downto 0);
			  weights: in std_logic_vector (63 downto 0);
			  bias: in std_logic_vector (31 downto 0);
			  zero_point: in std_logic_vector (31 downto 0);
			  multiplier: in std_logic_vector (31 downto 0);
			  shift: in std_logic_vector (4 downto 0);
			  output: out std_logic_vector (7 downto 0));
	end component;    
	
	component weight_rom IS
		GENERIC
		(
			file_path : string
		);
		PORT
		(
			address		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			clock		: IN STD_LOGIC;
			q		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
		);
	END component;
	
	component generic32_rom IS
		GENERIC
		(
			file_path : string
		);
		PORT
		(
			address		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
			clock		: IN STD_LOGIC  := '1';
			q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);
	END component;
	
	component generic5_rom IS
		GENERIC
		(
			file_path : string
		);
		PORT
		(
			address		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
			clock		: IN STD_LOGIC  := '1';
			q		: OUT STD_LOGIC_VECTOR (4 DOWNTO 0)
		);
	END component;

	type array_output is array (natural range <>) of std_logic_vector (7 downto 0);
	type array_weights is array (natural range <>) of std_logic_vector (63 downto 0);

	signal aggregate_output: array_output(7 downto 0);
	signal weights: array_weights(7 downto 0);
	
	signal bias, zero_point, multiplier: std_logic_vector (31 downto 0);
	signal shift:  std_logic_vector (4 downto 0);
	
begin

	cmod_0 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(0),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(0),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(0));
													
													
	wrom_0 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_0.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(0));
											
								
	cmod_1 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(1),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(1),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(1));
													
													
	wrom_1 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_1.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(1));
	
	cmod_2 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(2),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(2),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(2));
													
													
	wrom_2 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_2.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(2));
	
	cmod_3 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(3),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(3),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(3));
													
													
	wrom_3 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_3.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(3));
											
	cmod_4 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(4),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(4),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(4));
													
													
	wrom_4 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_4.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(4));
	
	
	cmod_5 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(5),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(5),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(5));
													
													
	wrom_5 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_5.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(5));
											
	
	cmod_6 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(6),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(6),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(6));
													
													
	wrom_6 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_6.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(6));
	
	
	cmod_7 : convolution_module_v2 port map(clk => clk,
													rd_en => rd_en(7),
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights(7),
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => aggregate_output(7));
													
													
	wrom_7 : weight_rom 	generic map(file_path => "F:\tcc\src\rom_files\weight_rom_7.mif")
								port map(clock => clk,
											address => weight_address,
											q => weights(7));
	
	bias_rom : generic32_rom 	generic map(file_path => "F:\tcc\src\rom_files\bias.mif")
										port map(clock => clk,
												address => parameters_adress,
												q => bias);
	
	zp_rom : generic32_rom 	generic map(file_path => "F:\tcc\src\rom_files\zeropoint.mif")
										port map(clock => clk,
												address => parameters_adress,
												q => zero_point);
												
	mult_rom : generic32_rom 	generic map(file_path => "F:\tcc\src\rom_files\mult.mif")
										port map(clock => clk,
												address => parameters_adress,
												q => multiplier);
	shift_rom : generic5_rom 	generic map(file_path => "F:\tcc\src\rom_files\shift.mif")
										port map(clock => clk,
												address => parameters_adress,
												q => shift);											
												
						
	
	generate_assigment: for i in 0 to 7 generate
		output((i*8)+7 downto i*8) <= aggregate_output(i);
	end generate generate_assigment;
	
	--output <= aggregate_output(0);

end architecture;
