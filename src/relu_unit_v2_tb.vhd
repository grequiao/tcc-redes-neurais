library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity relu_unit_v2_tb is
end entity;  

architecture rtl of relu_unit_v2_tb is

	component relu_unit_v2 is
		port(clk: in std_logic;
			  input: in signed(31 downto 0);
			  output: out signed (7 downto 0));
	end component;  
	

	signal clk: std_logic;
	signal input: signed (31 downto 0);
	signal output: signed (7 downto 0);
	
begin										
	uut : relu_unit_v2 port map(clk => clk,
										input => input,
										output => output);
										
	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		-- sincronizar com o clock
		wait for 10 ns;
		-- RELU
		input <= x"00000000";
		wait for 20 ns;
		input <= x"000000FF";
		wait for 20 ns;
		input <= x"000000F0";
		wait for 20 ns;
		input <= x"00000FF0";
		wait for 20 ns;
		input <= x"FF000000";
		wait for 20 ns;
		input <= x"FFFFFFFF";
		wait for 20 ns;
		input <= x"FFFFFFF0";
		wait for 20 ns;
		wait;
	end process;
	
	--a <= std_logic_vector(to_unsigned(counter, a'length));
	
end architecture;
