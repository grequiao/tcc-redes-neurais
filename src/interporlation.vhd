library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- realiza a operação a * alpha + b * (1 - alpha)

entity interporlation is
	port(clk: in std_logic;
		  a: in signed (15 downto 0);
		  b: in signed (15 downto 0);
		  alpha: in signed(15 downto 0);
		  output: out signed(31 downto 0));
end entity;  

architecture rtl of interporlation is

	
	signal a_reg, b_reg, alpha_reg : signed (15 downto 0);
	signal a_delay, b_delay, alpha_delay : signed (15 downto 0);
	signal alpha_sub : signed (16 downto 0);
	
	signal a_mult : signed (31 downto 0);
	signal b_mult : signed (32 downto 0);
	
	signal result : signed (33 downto 0);
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			a_reg <= a;
			b_reg <= b;
			alpha_reg <= alpha + "0000000000000000";
			alpha_sub <= "01000000000000000" - resize(alpha,17);
		end if;
	end process;
	
	--process(clk)
	--begin
	--	if rising_edge(clk) then
	--		a_delay <= a_reg;
	--		b_delay <= b_reg;
	--		alpha_delay <= alpha_reg;
	--		alpha_sub <= "01000000000000000" - resize(alpha_reg,17);
	--	end if;
	--end process;

	process(clk)
	begin
		if rising_edge(clk) then
			a_mult <= a_reg * alpha_reg;
			b_mult <= b_reg * alpha_sub;
		end if;
	end process;
	
	process(clk)
	begin
		if rising_edge(clk) then
			result <= resize(a_mult,34) + resize(b_mult,34);
		end if;
	end process;
	
	output <= result(31 downto 0);
	-- ajustar para fazer o clipping.

end architecture;
