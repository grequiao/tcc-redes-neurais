library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mac_unit is
	port(clk: in std_logic;
		  channels: in std_logic_vector (63 downto 0);
		  weights: in std_logic_vector (63 downto 0);
		  output: out std_logic_vector(18 downto 0));
end entity;  

architecture rtl of mac_unit is
	
	
	type vector_stage_0 is array (natural range <>) of signed(7 downto 0);
	type vector_stage_1 is array (natural range <>) of signed(15 downto 0);
	type vector_stage_2 is array (natural range <>) of signed(16 downto 0);
	type vector_stage_3 is array (natural range <>) of signed(17 downto 0);
	
	signal channels_reg, weights_reg: std_logic_vector (63 downto 0);
	signal stage_0 : vector_stage_0(15 downto 0);
	signal stage_1 : vector_stage_1(7 downto 0);
	signal stage_2 : vector_stage_2(3 downto 0);
	signal stage_3 : vector_stage_3(1 downto 0);
	signal stage_4 : signed(18 downto 0);
	
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			channels_reg <= channels;
			weights_reg <= weights;
		end if;
	end process;
	
	-- reajustar os sinais para ser mais facíl
	generate_assigment: for i in 0 to 7 generate
		stage_0(i) 	<= signed(channels_reg((i*8)+7 downto i*8));
		stage_0(i+8) <= signed(weights_reg((i*8)+7 downto i*8));
	end generate generate_assigment;
	
	
	generate_multiplication: for i in 0 to 7 generate
		process(clk)
		begin
			if rising_edge(clk) then
				stage_1(i) 	<= stage_0(i) * stage_0(i+8);
			end if;
		end process;
	end generate generate_multiplication;
	
	generate_add_1: for i in 0 to 3 generate
		process(clk)
		begin
			if rising_edge(clk) then
				stage_2(i) 	<= resize(stage_1(i),17) + resize(stage_1(i+4),17);
			end if;
		end process;
	end generate generate_add_1;
	
	generate_add_2: for i in 0 to 1 generate
		process(clk)
		begin
			if rising_edge(clk) then
				stage_3(i) 	<= resize(stage_2(i),18) + resize(stage_2(i+2),18);
			end if;
		end process;
	end generate generate_add_2;
	
	process(clk)
	begin
		if rising_edge(clk) then
			stage_4 	<= resize(stage_3(0),19) + resize(stage_3(1),19);
		end if;
	end process;
	
	output <= std_logic_vector(stage_4);

end architecture;
