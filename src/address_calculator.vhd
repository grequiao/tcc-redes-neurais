library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity address_calculator is
	port(clk: in std_logic;
		  ibase: in std_logic_vector (12 downto 0);
		  offset: in std_logic_vector (5 downto 0);
		  iWidth: in std_logic_vector(5 downto 0);
		  iLine: in std_logic_vector(5 downto 0);
		  address: out std_logic_vector(12 downto 0));
end entity;  

-- Realizar a conta
-- (ibase + offset) + (iWidth * iLine)

architecture rtl of address_calculator is
	

	signal base_reg : unsigned (12 downto 0);
	signal offset_reg : unsigned (5 downto 0);
	signal width_reg: unsigned (5 downto 0);
	signal line_reg: unsigned (5 downto 0);
	signal address_reg: unsigned (12 downto 0);
	
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			base_reg <= unsigned(ibase);
			offset_reg <= unsigned(offset);
			width_reg <= unsigned(iWidth);
			line_reg <= unsigned(iLine);
		end if;
	end process;
		
	
	-- realizar calculo
	process(clk)
	begin
		if rising_edge(clk) then
			address_reg <= offset_reg + base_reg + resize(width_reg * line_reg, 12);
		end if;
	end process;
	
	address <= std_logic_vector(address_reg);

end architecture;
