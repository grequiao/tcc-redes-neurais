library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- activation = '0' RELU, '1' SIGMOID

entity activation_unit_8bit is
	port(clk: in std_logic;
		  input: in  signed (31 downto 0);
		  output: out signed (24 downto 0));
end entity;  

architecture rtl of activation_unit_8bit is
	
	component activation_lut_8bit_impl is
	port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0));
	end component;  
		
	component interporlation_v2_8bits is
	port(clk: in std_logic;
		  a: in signed (7 downto 0);
		  b: in signed (7 downto 0);
		  alpha: in unsigned(15 downto 0);
		  output: out signed(24 downto 0));
	end component;  
	
	signal activation: std_logic_vector(9 downto 0);
	signal alpha_delay_1, alpha_delay_2, alpha_delay_3: unsigned(15 downto 0);
	signal a, b: signed (7 downto 0);
	signal clamped: signed (31 downto 0);
	signal addition: signed (31 downto 0);
	
	-- clamp para 26 bits
	constant max_value: signed(31 downto 0) := "00000001111111111111111111111111";
	constant min_value: signed(31 downto 0) := "11111110000000000000000000000000";

	-- constante do Terceiro ZeroPoint
	constant Z3: signed(31 downto 0) := x"0000009C"; 
begin

	lut : activation_lut_8bit_impl port map(clk => clk,
								a => activation,
								out_a => a,
								out_b => b);
								
	inter : interporlation_v2_8bits port map(clk => clk,
								a => a,
								b => b,
								alpha => alpha_delay_3,
								output => output);
	
	-- Realizar soma do Z3
	process(clk)
	begin
		if rising_edge(clk) then
			addition <= input + Z3;
		end if;
	end process;
	
	
	-- Realizar Clamp
	process(clk)
	variable multMem: signed(31 downto 0);
	begin
		if rising_edge(clk) then
			multMem := addition;
			if multMem > max_value then
				multMem := max_value;
			elsif multMem < min_value then
				multMem := min_value;
			end if;
			clamped <= multMem;
		end if;
	end process;
	
	-- Realizar assignments
	activation <= std_logic_vector(clamped(25 downto 16));
	
	-- Realizar delays
	process(clk)
	begin
		if rising_edge(clk) then
			alpha_delay_1 <= unsigned(clamped(15 downto 0));
			alpha_delay_2 <= alpha_delay_1;
			alpha_delay_3 <= alpha_delay_2;
		end if;
	end process;
		
end architecture;
