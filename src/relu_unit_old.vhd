library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Realizar max(x, 0)
-- Assumindo que Zero Point -> 0

entity relu_unit_old is
	port(clk: in std_logic;
		  input: in signed(31 downto 0);
		  output: out signed (31 downto 0));
end entity;  

architecture rtl of relu_unit_old is
	
	signal relu_reg: signed(31 downto 0) := (others => '0');
	
begin

	-- não registrar as entradas
	
	process(clk)
	begin
		if rising_edge(clk) then
			if input > 0 then
				relu_reg <= input;
			else
				relu_reg <= (others => '0');
			end if;
		end if;
	end process;
	
	
	output <= relu_reg;

end architecture;
