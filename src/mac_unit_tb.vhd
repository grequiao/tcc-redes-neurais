library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mac_unit_tb is
end entity;  

architecture arch_mac_unit_tb of mac_unit_tb is
	component mac_unit is
		port(clk: in std_logic;
			  channels: in std_logic_vector (63 downto 0);
			  weights: in std_logic_vector (63 downto 0);
			  output: out std_logic_vector(18 downto 0));
	end component;   
	
	signal clk : std_logic;
	signal output: std_logic_vector(18 downto 0);
	signal a,b: std_logic_vector(63 downto 0);
begin
	uut : mac_unit port map(clk => clk,
										channels => a,
										weights => b,
										output => output);
								
	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;

	process
	begin
		a <= "0000111100000000000000010000001000000011100000011000100011000010";
		b <= "1111111111111111111111111111111111111111111111111111111111111111";
		wait for 20 ns;
		b <= "0001001001110100111001000000101001110101110010010010011001110100";
		a <= "1010100101100110111011110101001111000111010111011101110101010100";
		wait for 20 ns;
		b <= "1011000011011110101011001001000001000101011111000011011000001010";
		a <= "1000000101111110000000111010000100110010010110001100100100011011";
		wait for 20 ns;
		b <= "1100010100000110011010001101011100010011000110001111010000011111";
		a <= "1100000111101010010010010011001001010011101111011000110010111010";
		wait for 20 ns;
		b <= "1010111101010100001001100000000011000110000111010101000000000000";
		a <= "0010001011111000100000010001000001110111000011100110111101000111";
		wait;
	end process;
	
end architecture;
