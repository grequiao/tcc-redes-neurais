library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity accumulator_bank is
	port(clk: in std_logic;
		  load: in std_logic;
		  rd_en: in std_logic;
		  accumulate: in std_logic;
		  selector: in std_logic_vector(1 downto 0);
		  input: in signed (18 downto 0);
		  data: in signed(31 downto 0);
		  output: out signed(31 downto 0));
end entity;  

architecture rtl of accumulator_bank is
	
	type acc_bank is array (natural range <>) of signed (31 downto 0);
	
	--signal acc: signed (31 downto 0);
	signal acc: acc_bank(4 downto 0);
	signal op1, op2, data_reg: signed (31 downto 0);	
begin
	
	op1 <= data_reg when load = '1' else
			 acc(to_integer(unsigned(selector)));
	
	op2 <= resize(input, 32) when accumulate = '1' else
				(others=>'0');

	process(clk)
	begin
		if rising_edge(clk) then
			acc(to_integer(unsigned(selector))) <= op1 + op2;
			if rd_en = '1' then
				data_reg <= data;
			end if;
		end if;
	end process;
	
	output <= acc(to_integer(unsigned(selector)));

end architecture;
