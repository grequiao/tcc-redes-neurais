library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity agu_weights_tb is
end entity;  

architecture rtl of agu_weights_tb is

	component activation_lut_8bit_impl
		port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0)
		);
	end component;
	
	component agu_weights is
		port(clk: in std_logic;
		  baseAdress: in std_logic_vector (9 downto 0);
		  currentFilter: in std_logic_vector(2 downto 0);
		  kernelSize: in std_logic_vector(6 downto 0);
		  positionKernel: in std_logic_vector(6 downto 0);
		  address: out std_logic_vector(9 downto 0));
	end component;  

	
	signal clk: std_logic;
	signal baseAdress, address: std_logic_vector(9 downto 0);
	signal currentFilter: std_logic_vector(2 downto 0);
	signal kernelSize, positionKernel: std_logic_vector(6 downto 0);
	
	--signal test: unsigned(7 downto 0);
	
	signal counter: integer range 0 to 1024 := 0;
	
	signal kernel_counter: integer range 0 to 128 := 0;
	signal filter_counter: integer range 0 to 32 := 0;
	
begin										
	uut : agu_weights port map(clk => clk,
										baseAdress => baseAdress,
										currentFilter => currentFilter,
										kernelSize => kernelSize,
										positionKernel => positionKernel,
										address => address);
												
	process
	begin
		clk <= '0';
		
		if kernel_counter /= 8 then
		--if counter /= 1023 then
			kernel_counter <= kernel_counter + 1;
		else
			kernel_counter <= 0;
			filter_counter <= filter_counter + 1;
			if filter_counter = 31 then
				filter_counter <= 0;
			end if;
		end if;		
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	
	process
	begin
		baseAdress <= "0000000000";
		kernelSize  <= "0001001";
		wait;
	end process;
	
	positionKernel <= std_logic_vector(to_unsigned(kernel_counter, positionKernel'length));
	currentFilter <= std_logic_vector(to_unsigned(filter_counter, currentFilter'length));
	
end architecture;
