library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity activation_lut_8bit_impl is
	port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  --b: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0));
end entity;  

architecture rtl of activation_lut_8bit_impl is

	component activation_lut_8bit
		PORT
		(
			address_a		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			address_b		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			clock		: IN STD_LOGIC  := '1';
			q_a		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
			q_b		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
		);
	end component;
	
	constant additional_lut : std_logic_vector(7 downto 0) := x"7E"; -- -> 126 ou 4.0 quantizado no range -4 a 4
	signal o_a, o_b, r_b: std_logic_vector(7 downto 0);
	signal delay_o_a, delay_r_b: std_logic_vector(7 downto 0);
	signal b, b_reg, b2_reg: std_logic_vector(9 downto 0);
begin
	ROM : activation_lut_8bit port map(clock => clk,
												address_a => a,
												address_b => b,
												q_a => o_a,
												q_b => o_b);

	b <= std_logic_vector(unsigned(a) + 1);
	
	-- sincronizar com o ROM
	process(clk)
	begin
		if rising_edge(clk) then
			b_reg <= b;
			b2_reg <= b_reg;
		end if;
	end process;
	
	r_b <= additional_lut when b2_reg = "1000000000" else
			 o_b;
			 
	-- delays devido ao DSP
	process(clk)
	begin
		if rising_edge(clk) then
			delay_o_a <= o_a;
			delay_r_b <= r_b;
		end if;
	end process;
	
	--out_a <= signed(o_a);
	--out_b <= signed(r_b);
	
	out_a <= signed(delay_o_a);
	out_b <= signed(delay_r_b);

end architecture;
