library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- AGU -> Address Generation Unit
entity agu_weights is
	port(clk: in std_logic;
		  baseAdress: in std_logic_vector (9 downto 0);
		  currentFilter: in std_logic_vector(2 downto 0);
		  kernelSize: in std_logic_vector(6 downto 0);
		  positionKernel: in std_logic_vector(6 downto 0);
		  address: out std_logic_vector(9 downto 0));
end entity;  

-- Realizar a conta
-- baseAdress + currentFilter * kernelSize + positionKernel


architecture rtl of agu_weights is
	

	signal base_reg : unsigned (9 downto 0);
	signal cf_reg : unsigned (2 downto 0); 				-- currentFilter
	signal ks_reg: unsigned (6 downto 0);  				-- kernelSize
	signal pk_reg, pk_reg_1: unsigned (6 downto 0);  	-- positionKernel
	signal address_reg: unsigned (9 downto 0);
	
	signal mult_add: unsigned (9 downto 0);
		
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			base_reg <= unsigned(baseAdress);
			cf_reg <= unsigned(currentFilter);
			ks_reg <= unsigned(kernelSize);
			pk_reg <= unsigned(positionKernel);
		end if;
	end process;
		
	
	-- realizar calculo
	process(clk)
	begin
		if rising_edge(clk) then
			--address_reg <= base_reg + cf_reg * ks_reg + pk_reg;
			address_reg <= mult_add + pk_reg_1;		
			mult_add <= base_reg + cf_reg * ks_reg;
			
			--registrador de pipeline
			pk_reg_1 <= pk_reg;
			
			address <= std_logic_vector(address_reg);
		end if;
	end process;
	
	--address <= std_logic_vector(address_reg);

end architecture;
