library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- activation = '0' RELU, '1' SIGMOID

entity convolution_module is
	port(clk: in std_logic;
		  rd_en: in std_logic;
		  activation: in std_logic; -- '0' RELU, '1' SIGMOID
		  load: in std_logic;
		  accumulate: in std_logic;
		  channels: in std_logic_vector (63 downto 0);
		  weights: in std_logic_vector (63 downto 0);
		  bias: in std_logic_vector (31 downto 0);
		  zero_point: in std_logic_vector (31 downto 0);
		  multiplier: in std_logic_vector (31 downto 0);
		  shift: in std_logic_vector (4 downto 0);
		  output: out std_logic_vector (7 downto 0));
end entity;  

architecture rtl of convolution_module is
	component mac_unit is
		port(clk: in std_logic;
			  channels: in std_logic_vector (63 downto 0);
			  weights: in std_logic_vector (63 downto 0);
			  output: out std_logic_vector(18 downto 0));
	end component;  
	
	component accumulator is
		port(clk: in std_logic;
			  load: in std_logic;
			  rd_en: in std_logic;
			  accumulate: in std_logic;
			  input: in signed (18 downto 0);
			  data: in signed(31 downto 0);
			  output: out signed(31 downto 0));
	end component;  
	
	component converter_v2 is
	port(clk: in std_logic;
		  convert: in signed (31 downto 0);
		  offset: in signed (31 downto 0);
		  multiplier: in signed(31 downto 0);
		  shiftamount: in std_logic_vector (4 downto 0);
		  rd_en: std_logic;
		  output: out signed(31 downto 0));
	end component; 
		
	component relu_unit is
		port(clk: in std_logic;
			  rd_en: in std_logic;
			  input: in signed(31 downto 0);
			  zero_point: in signed(31 downto 0);
			  output: out signed (31 downto 0));
	end component; 
		
	component activation_unit is
	port(clk: in std_logic;
		  input: in  signed (31 downto 0);
		  output: out signed (31 downto 0));
	end component;  
	
	component activation_unit_8bit is
	port(clk: in std_logic;
		  input: in  signed (31 downto 0);
		  output: out signed (24 downto 0));
	end component;  


	
	signal mac_result: std_logic_vector(18 downto 0);
	signal acc_result: signed(31 downto 0);
	signal cov_result: signed(31 downto 0);
	signal relu_result:signed(31 downto 0);
	--signal act_result: signed(31 downto 0);
	signal act_result: signed(24 downto 0);

	
	signal select_1: signed(31 downto 0);
	signal select_2: signed(7 downto 0);
	
	-- Constante do Relu como é [0, 1] é -128
	constant RELU_ZP: signed(31 downto 0) := x"FFFFFF80";
	
	signal relu_add_zp: signed(31 downto 0);
	signal relu_clamp: signed(31 downto 0);
	
	-- clamp do RELU para [-128, 127]
	constant min_value: signed(31 downto 0) := x"FFFFFF80";
	constant max_value: signed(31 downto 0) := x"0000007F";
begin

	mac : mac_unit port map(clk => clk,
									channels => channels,
									weights => weights,
									output => mac_result);
									
									
	acc : accumulator port map(clk => clk,
										load  => load,
										rd_en => rd_en,
										accumulate => accumulate,
										input => signed(mac_result),
										data => signed(bias),
										output => acc_result);
										
	select_1 <= acc_result when activation = '1' else
					relu_result;
										
	cov : converter_v2 port map(clk => clk,
										convert  => select_1,
										offset => signed(zero_point),
										multiplier => signed(multiplier),
										shiftamount => shift,
										rd_en => rd_en,
										output => cov_result);
										
	relu: relu_unit port map(clk => clk,
									 rd_en => rd_en,
									 input  => acc_result,
									 zero_point => signed(zero_point),
									 output => relu_result);
									 
									 
	--act: activation_unit port map(clk => clk,
	--								 input  => cov_result,
	--								 output => act_result);
									 
	act: activation_unit_8bit port map(clk => clk,
									 input  => cov_result,
									 output => act_result);
	
	--select_2 <= cov_result
	
	-- Somar +128 para resultado do RELU.
	process(clk)
	begin
		if rising_edge(clk) then
			relu_add_zp <= cov_result + RELU_ZP;
		end if;
	end process;
	
	
	-- Realizar Clamp no resultado convertido do RELU
	process(clk)
	variable multMem: signed(31 downto 0);
	begin
		if rising_edge(clk) then
			multMem := relu_add_zp;
			if multMem > max_value then
				multMem := max_value;
			elsif multMem < min_value then
				multMem := min_value;
			end if;
			relu_clamp <= multMem;
		end if;
	end process;
	
	-- Select para output
	select_2 <= relu_clamp(7 downto 0) when activation = '0' else
					act_result(24 downto 17);
					--act_result(31 downto 24);

	--output<= (others => '0');
	output <= std_logic_vector(select_2);
end architecture;
