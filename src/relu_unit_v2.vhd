library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Assumindo que Zero Point -> -128

entity relu_unit_v2 is
	port(clk: in std_logic;
		  input: in signed(31 downto 0);
		  output: out signed (7 downto 0));
end entity;  

architecture rtl of relu_unit_v2 is
	
	signal relu_reg, input_reg: signed(31 downto 0) := (others => '0');
	signal relu_zp_reg: signed(7 downto 0) := (others => '0');	
	
	-- ZP fixo para -128
	constant fixed_zp: signed(7 downto 0) := x"80";
	
	constant min_value: signed(31 downto 0) := x"00000000";
	constant max_value: signed(31 downto 0) := x"000000FF";
	--constant max_value: signed(31 downto 0) := x"0000007F";

	
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			input_reg <= input;
		end if;
	end process;
	
	
	-- min(0, max(127, x))
	process(clk)
	variable multMem: signed(31 downto 0);
	begin
		if rising_edge(clk) then
			multMem := input_reg;
			if multMem > max_value then
				multMem := max_value;
			elsif multMem < min_value then
				multMem := min_value;
			end if;
			relu_reg <= multMem;
		end if;
	end process;
		
	-- relu_reg + zp(i.e. -128)
	process(clk)
	begin
		if rising_edge(clk) then
			--relu_zp_reg <= signed(unsigned(relu_reg(7 downto 0)) + unsigned(fixed_zp));
			relu_zp_reg <= relu_reg(7 downto 0) + fixed_zp;
		end if;
	end process;
	
	
	output <= relu_zp_reg;

end architecture;
