library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- realiza a operação a * alpha + b * (alpha - 1)

entity interporlation_test is
	port(clk: in std_logic;
		  a: in std_logic_vector (15 downto 0);
		  b: in std_logic_vector (15 downto 0);
		  alpha: in std_logic_vector(15 downto 0);
		  output: out std_logic_vector(33 downto 0));
end entity;  

architecture rtl of interporlation_test is
	
	component mult_add is
	port (
		result  : out std_logic_vector(33 downto 0);                    --  result.result
		dataa_0 : in  std_logic_vector(15 downto 0); 
		dataa_1 : in  std_logic_vector(15 downto 0);
		datab_0 : in  std_logic_vector(15 downto 0); -- datab_0.datab_0
		datab_1 : in  std_logic_vector(15 downto 0); -- datab_1.datab_1
		clock0  : in  std_logic;             --  clock0.clock0
		datac_0 : in  std_logic_vector(15 downto 0); -- datac_0.datac_0
		datac_1 : in  std_logic_vector(15 downto 0)  -- datac_1.datac_1
	);
	end component;

begin

	unit : mult_add port map(result => output,
									dataa_0 => alpha,
									dataa_1 => alpha,
									datab_0 => a,
									datab_1 => b,
									clock0  => clk,
									datac_0 => "0000000000000000",
									datac_1 => "0100000000000000");

end architecture;
