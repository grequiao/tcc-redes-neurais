library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity activation_lut_8bit_impl_tb is
end entity;  

architecture rtl of activation_lut_8bit_impl_tb is

	component activation_lut_8bit_impl
		port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0)
		);
	end component;
	
	signal clk: std_logic;
	signal a : std_logic_vector(9 downto 0);
	signal o_a, o_b: signed(7 downto 0);
	
	signal counter: integer range 0 to 1024 := 0;
	
begin
	uut : activation_lut_8bit_impl port map(clk => clk,
										a => a,
										out_a => o_a,
										out_b => o_b);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
	if counter /= 1023 then
	--if counter /= 1023 then
			counter <= counter + 1;
		else
			counter <= 0;
		end if;
		wait for 20 ns;
	end process;
	
	a <= std_logic_vector(to_unsigned(counter, a'length));
	
end architecture;
