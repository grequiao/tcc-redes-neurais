library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity activation_lut_tb is
end entity;  

architecture rtl of activation_lut_tb is

	component activation_lut
		PORT
		(
			address_a		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			address_b		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			clock		: IN STD_LOGIC  := '1';
			q_a		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
			q_b		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
		);
	end component;
	
	signal clk: std_logic;
	signal a, b: std_logic_vector(9 downto 0);
	signal o_a, o_b: std_logic_vector(15 downto 0);
	
	signal counter: integer range 0 to 1024 := 0;
	
begin
	uut : activation_lut port map(clock => clk,
										address_a => a,
										address_b => b,
										q_a => o_a,
										q_b => o_b);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		if counter /= 1023 then
			counter <= counter + 1;
		else
			counter <= 0;
		end if;
		wait for 20 ns;
	end process;
	
	a <= std_logic_vector(to_unsigned(counter, a'length));
	b <= std_logic_vector(to_unsigned(counter, b'length));
end architecture;
