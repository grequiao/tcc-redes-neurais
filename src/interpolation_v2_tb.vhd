library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity interpolation_v2_tb is
end entity;  

architecture rtl of interpolation_v2_tb is
	component interporlation_v2 is
		port(clk: in std_logic;
		  a: in signed (15 downto 0);
		  b: in signed (15 downto 0);
		  alpha: in unsigned(15 downto 0);
		  output: out signed(31 downto 0));
	end component;  
	
	signal clk: std_logic;
	signal a, b: signed(15 downto 0);
	signal alpha: unsigned(15 downto 0);
	signal output: signed(31 downto 0);
	
begin
	uut : interporlation_v2 port map(clk => clk,
										a => a,
										b => b,
										alpha => alpha,
										output => output);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		a <= x"0032";
		b <= x"0064";
		alpha <= x"7FFF";
		wait for 100 ns;
		a <= x"0064";
		b <= x"00C8";
		wait for 100 ns;
		a <= x"FFFF";
		b <= x"0000";
		wait for 100 ns;
		a <= x"FFFE";
		b <= x"FFFC";
		wait for 100 ns;
		alpha <= x"FFFF";
		wait for 100 ns;
		a <= x"8000";
		wait for 100 ns;
		a <= x"0032";
		b <= x"0064";
		alpha <= x"7FF0";
		wait;
	end process;
end architecture;
