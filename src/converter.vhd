library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity converter is
	port(clk: in std_logic;
		  convert: in signed (31 downto 0);
		  offset: in signed (31 downto 0);
		  multiplier: in signed(31 downto 0);
		  shiftamount: in std_logic_vector (4 downto 0);
		  output: out signed(31 downto 0));
end entity;  

-- Realiza (X - Z) * S * 2^-M

architecture rtl of converter is

	component arith_shifter_barrel is
	  port (
			clk					: in  std_logic;
			Input					: in	signed(31 downto 0);
			ShiftAmount			: in	std_logic_vector(4 downto 0);
			Output				: out	signed(31 downto 0)
		);
	end component;
	
	signal input_reg, offset_reg, multiplier_reg : signed (31 downto 0);
	signal offset_op, offset_op_2 : signed (32 downto 0);
	signal mult_op, mult_op2 : signed(31 downto 0);
	signal shift_op: signed(31 downto 0);
	
	signal mult_intermediate: signed(64 downto 0);
	
	signal shiftamount_reg_1, shiftamount_reg_2, shiftamount_reg_3, shiftamount_reg_4, test : std_logic_vector (4 downto 0);
	signal mul_reg_pipe : signed (31 downto 0);
	
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			input_reg <= convert;
			offset_reg <= offset;
			multiplier_reg <= multiplier;
			shiftamount_reg_1 <= shiftamount;
		end if;
	end process;
	

	-- subtração
	process(clk)
	begin
		if rising_edge(clk) then
			offset_op <= resize(input_reg, 33) - resize(offset_reg, 33);			-- registrador da LUT/LAB
			offset_op_2 <= offset_op;															-- registrador da entrada do DSP
			
			test <= shiftamount_reg_1;
			shiftamount_reg_2 <= test;
			
			mul_reg_pipe <= multiplier_reg;													-- Delay / Chega na entrada do DSP
		end if;
	end process;
	
	mult_intermediate <= offset_op_2 * mul_reg_pipe;
	
	process(clk)
	begin
		if rising_edge(clk) then
			mult_op <= mult_intermediate(64 downto 33);
			shiftamount_reg_3 <= shiftamount_reg_2;
			
			
			mult_op2 <= mult_op;
			shiftamount_reg_4 <= shiftamount_reg_3;
		end if;
	end process;
	
	
	shifter : arith_shifter_barrel port map(clk => clk,
														Input => mult_op2,
														ShiftAmount => shiftamount_reg_4,
														output => shift_op);
	
	output <= shift_op;


end architecture;
