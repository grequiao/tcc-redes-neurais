library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- realiza a operação a * alpha + b * (1 - alpha)

entity interporlation_v2_8bits_old is
	port(clk: in std_logic;
		  a: in signed (7 downto 0);
		  b: in signed (7 downto 0);
		  alpha: in unsigned(15 downto 0);
		  output: out signed(24 downto 0));
end entity;  

architecture rtl of interporlation_v2_8bits_old is

	
	signal a_reg, b_reg : signed (7 downto 0);
	signal alpha_sub, alpha_reg : unsigned (15 downto 0);
	
	signal a_mult : signed (24 downto 0);
	signal b_mult : signed (24 downto 0);
	
	signal result : signed (24 downto 0);
begin

	-- registrar as entradas
	process(clk)
	begin
		if rising_edge(clk) then
			a_reg <= a;
			b_reg <= b;
			alpha_reg <= alpha;
			alpha_sub <= x"FFFF" - alpha;
		end if;
	end process;
	
	--process(clk)
	--begin
	--	if rising_edge(clk) then
	--		a_delay <= a_reg;
	--		b_delay <= b_reg;
	--		alpha_delay <= alpha_reg;
	--		alpha_sub <= "01000000000000000" - resize(alpha_reg,17);
	--	end if;
	--end process;

	process(clk)
	begin
		if rising_edge(clk) then
			a_mult <= a_reg * signed(resize(alpha_reg, 17));
			b_mult <= b_reg * signed(resize(alpha_sub, 17));
		end if;
	end process;
	
	process(clk)
	begin
		if rising_edge(clk) then
			result <= a_mult + b_mult;
		end if;
	end process;
	
	output <= result;
	-- ajustar para fazer o clipping.
	-- Nesse não é necessário fazer o clipping
	-- apenas no interpolation_v2 original

end architecture;
