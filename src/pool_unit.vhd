library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pool_unit is
	port(clk: in std_logic;
		  rst: in std_logic;
		  en: in std_logic;
		  channels: in std_logic_vector (63 downto 0);
		  output: out std_logic_vector (63 downto 0));
end entity;  

architecture rtl of pool_unit is
	
	
	type vector is array (natural range <>) of signed(7 downto 0);
	
	signal incoming_value: vector(7 downto 0);
	signal vector_reg : vector(7 downto 0) := (others => (x"80"));
	signal intermediary : std_logic_vector (63 downto 0);
	
begin

	-- não registrar as entradas
	
	
	-- reajustar os sinais para ser mais facíl
	generate_assigment: for i in 0 to 7 generate
		incoming_value(i) 	<= signed(channels((i*8)+7 downto i*8));
		intermediary((i*8)+7 downto i*8) <= std_logic_vector(vector_reg(i));
	end generate generate_assigment;
	
	
	generate_max: for i in 0 to 7 generate
		process(clk)
		begin
			if rising_edge(clk) then
				if rst = '1' then
					vector_reg(i) <= x"80"; -- Setar para valor mínimo -> -128
				else
					if incoming_value(i) > vector_reg(i) then
						vector_reg(i) <= incoming_value(i);
					end if;
				end if;
			end if;
		end process;
	end generate generate_max;
	
	
	output <= intermediary when en = '1' else 
					(others => 'Z');

end architecture;
