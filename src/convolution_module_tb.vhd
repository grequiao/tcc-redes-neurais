library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity convolution_module_tb is
end entity;  

architecture rtl of convolution_module_tb is

	component activation_lut_8bit_impl
		port(clk: in std_logic;
		  a: in std_logic_vector(9 downto 0);
		  out_a: out signed (7 downto 0);
		  out_b: out signed (7 downto 0)
		);
	end component;
	
	component convolution_module is
		port(clk: in std_logic;
			  rd_en: in std_logic;
			  activation: in std_logic; -- '0' RELU, '1' SIGMOID
			  load: in std_logic;
			  accumulate: in std_logic;
			  channels: in std_logic_vector (63 downto 0);
			  weights: in std_logic_vector (63 downto 0);
			  bias: in std_logic_vector (31 downto 0);
			  zero_point: in std_logic_vector (31 downto 0);
			  multiplier: in std_logic_vector (31 downto 0);
			  shift: in std_logic_vector (4 downto 0);
			  output: out std_logic_vector (7 downto 0));
	end component;  
	
	signal clk, rd_en, activation, load, accumulate: std_logic;
	signal channels, weights: std_logic_vector (63 downto 0);
	signal bias, zero_point, multiplier: std_logic_vector (31 downto 0);
	signal shift: std_logic_vector (4 downto 0);
	signal output: std_logic_vector (7 downto 0);
	
	signal a : std_logic_vector(9 downto 0);
	signal o_a, o_b: signed(7 downto 0);
	signal counter: integer range 0 to 1024 := 0;
	
begin										
	uut : convolution_module port map(clk => clk,
													rd_en => rd_en,
													activation => activation,
													load => load,
													accumulate => accumulate,
													channels => channels,
													weights => weights,
													bias => bias,
													zero_point => zero_point,
													multiplier => multiplier,
													shift => shift,
													output => output);
										
	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		-- sincronizar com o clock
		wait for 10 ns;
		-- RELU
		activation <= '0';
		accumulate <= '0';
		-- valores iniciais
		-- aleatorios
		channels <= "0001111110001010011000011000001011000001001100110000100111001111";
		weights <= "1000101110111101010101100100101011011000011010001011110001101100";
		bias <= "00000000000000010101100001001000";
		zero_point <= "11110110010110101111111011000101";
		multiplier <= "10101101100110111001001111011010";
		shift <= "00101";
		-- ativar a leitura
		rd_en <= '1';
		wait for 20 ns;
		-- desativar
		rd_en <= '0';
		
		channels <= "1001011100011001011001100110000000110000100001110100011001010000";
		weights <= "1110111100010100101101110111100100101011110001111001110000000111";
		bias <= "00100110011010000010000010010110";
		zero_point <= "10111001100110111011011001000111";
		multiplier <= "01101111110000100110101011000110";
		shift <= "00011";
		--load <= '1';
		
		wait for 20 ns;
		-- carregar valor no acc.
		load <= '1';
		wait for 40 ns;
		load <= '0';
		-- Começar a acumular
		accumulate <= '1';
		wait for 400 ns;
		-- Mudar para sigmoid
		activation <= '1';
		load <= '1';
		wait for 20 ns;
		load <= '0';
		wait;
	end process;
	
	--a <= std_logic_vector(to_unsigned(counter, a'length));
	
end architecture;
