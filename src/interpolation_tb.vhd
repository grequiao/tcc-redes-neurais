library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity interpolation_tb is
end entity;  

architecture rtl of interpolation_tb is
	component interporlation is
		port(clk: in std_logic;
		  a: in signed (15 downto 0);
		  b: in signed (15 downto 0);
		  alpha: in signed(15 downto 0);
		  output: out signed(31 downto 0));
	end component;  
	
	signal clk: std_logic;
	signal a, b, alpha: signed(15 downto 0);
	signal output: signed(31 downto 0);
	
begin
	uut : interporlation port map(clk => clk,
										a => a,
										b => b,
										alpha => alpha,
										output => output);

	process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process;
	
	process
	begin
		a <= x"0032";
		b <= x"0064";
		alpha <= "0100000000000000";
		wait for 1000 ns;
		a <= x"0064";
		b <= x"00C8";
		wait;
	end process;
end architecture;
