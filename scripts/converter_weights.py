# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 20:42:16 2024

@author: Gabriel
"""

import mif
import math
from functools import reduce
import numpy as np

def array_to_64bit(x):
    assert x.shape[0] % 8 == 0
    
    cover = x.shape[0] / 8
    list_array = np.split(x, cover)
    list_array = [to_64bit(i) for i in list_array]
    return np.array(list_array, dtype='int64')

def to_64bit(x):
    #converter para int64
    typed = x.astype('int64')
    
# =============================================================================
#     number = typed[0] & 255
#     number |= (typed[1] & 255) << 8 
#     number |= (typed[2] & 255) << 16
#     number |= (typed[3] & 255) << 24
#     number |= (typed[4] & 255) << 32
#     number |= (typed[5] & 255) << 40
#     number |= (typed[6] & 255) << 48
#     number |= (typed[7] & 255) << 56
# =============================================================================
    
    number = reduce(lambda x,y: (y & 255) | (x << 8), typed[::-1])
    
    return number

# =============================================================================
# weights_c0 = np.load("weights_c0.npy")
# divided = [weights_c0[i::8] for i in range(0, 8)]
# 
# pad_func = lambda x: np.pad(x, [(0, 0), (0, 0), (0,0), (0,7)], mode='constant', constant_values=0)
# 
# divided = list(map(pad_func, divided))
# 
# flat_map = []
# 
# for i in divided:
#     flat_memory = []
#     for filterKernel in i:
#         for jar in filterKernel:
#             for k in jar:
#                 flat_memory = np.append(flat_memory, array_to_64bit(k))
#     flat_map.append(flat_memory)
# =============================================================================
    

def convert_convolution_weights(memory, fileName, padding=False):
    weights = np.load(fileName)
    divided = [weights[i::8] for i in range(0, 8)]
    
    if padding:
        pad_func = lambda x: np.pad(x, [(0, 0), (0, 0), (0,0), (0,7)], mode='constant', constant_values=0)
        divided = list(map(pad_func, divided))

    flat_map = []

    for i in divided:
        flat_memory = np.array([], dtype='int64')
        for filterKernel in i:
            for jar in filterKernel:
                for k in jar:
                    flat_memory = np.append(flat_memory, array_to_64bit(k))
        flat_map.append(flat_memory)
    return [np.append(i, j) for i, j in zip(memory, flat_map)]

def convert_fc_weights(memory, fileName):
    weights = np.load(fileName)
    divided = [weights[i::8] for i in range(0, 8)]
    
    flat_map = []

    for i in divided:
        flat_memory = np.array([], dtype='int64')
        for matrix in i:
            flat_memory = np.append(flat_memory, array_to_64bit(matrix))
        flat_map.append(flat_memory)
    return [np.append(i, j) for i, j in zip(memory, flat_map)]
    
    

memory = [np.array([], dtype='int64') for i in range(0, 8)]
memory = convert_convolution_weights(memory, "weights_c0.npy", padding=True)
memory = convert_convolution_weights(memory, "weights_c1.npy")
memory = convert_convolution_weights(memory, "weights_c2.npy")
memory = convert_fc_weights(memory, "weights_f.npy")

#weights_f = np.load("weights_f.npy")


test = np.array([-10, -25,0,0,0,0,0,0], dtype='int64')
to_64bit(test)

#np.binary_repr(d & 255, width=8)
#test[1] << 8 | (test[0] & 255)

with open(".\\weights\\base_8bit.mif") as f:
    mem = mif.load(f)
    
def to_bin(x):
    x = np.uint64(x)
    try:
        #if(x < 0):
        #    return f'{18446744073709551615+x+1:064b}'
        return f'{x:064b}'
    except Exception as ex:
        print(x)
        print(type(x))
        print(ex)
        
    
mem = []
for i in memory:
    binary = [[*(to_bin(j)[::-1])] for j in i]
    #converter strings para numero
    binary = [[int(i) for i in j] for j in binary]
    mem.append(np.array(binary, dtype='uint8'))
    
for count, ele in enumerate(mem):
    with open(f'.\\weights\\weights_rom_{count}.mif', 'w') as f:
        mif.dump(ele, f, address_radix='UNS', data_radix='UNS')
    




