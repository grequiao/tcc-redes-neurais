# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 21:33:42 2023

@author: Gabriel
"""

import math

def calc_scale(max_value, min_value, bits):
    return (max_value - min_value) / (2**bits-1)

s1 = 0.0115
x = 5
zq = -50

print("valor original: " + str((x+zq)*s1))

s2 = 0.0555
zq2 = 0

frexp_result = math.frexp(s1/s2)
print("Resultado do frexp:" + str(frexp_result))


scale = calc_scale(1, -1, 32)
scale = 1 / 2**31
print("scale: "+ str(scale))
mult = round(frexp_result[0] / scale)
shift = frexp_result[1]
print("multiplicador: " + str(mult))

resultado = (x+zq) * mult * (2**shift)
print("resultado quantizado(antes do shift): " + str(resultado))

resultado = round(resultado) >> 31
print("resultado quantizado: " + str(resultado))
resultado = resultado * s2
print("resultado dequantizado: " + str(resultado))