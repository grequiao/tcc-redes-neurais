# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 20:20:20 2024

@author: Gabriel
"""

import mif
import math
from functools import reduce
import numpy as np

zp_relu = 128

def load_memory(fileName, memory):
    load = np.load(fileName)
    return np.append(memory, load)

memory = np.array([], dtype='int32')
memory = load_memory(".//bias//bias_c0.npy", memory)
memory = load_memory(".//bias//bias_c1.npy", memory)
memory = load_memory(".//bias//bias_c2.npy", memory)
memory = load_memory(".//bias//bias_fc.npy", memory)

with open(".\\weights\\base_8bit.mif") as f:
    mem = mif.load(f)

def to_32_bit_bin(x):
    x = np.uint32(x)
    try:
        #if(x < 0):
        #    return f'{18446744073709551615+x+1:064b}'
        return f'{x:032b}'
    except Exception as ex:
        print(x)
        print(type(x))
        print(ex)
        
def convert_to_multiple_bits_32(number):
    binary = [*(to_32_bit_bin(number)[::-1])]
    return [int(i) for i in binary]

mem = np.array([convert_to_multiple_bits_32(i) for i in memory], dtype='uint8')

with open('.\\bias\\bias.mif', 'w') as f:
    mif.dump(mem, f, address_radix='UNS', data_radix='UNS')
    

def load_memory_zp(fileName, memory):
    load = np.load(fileName)
    #load = np.sum(load, axis=(1,2,3))
    load = np.sum(load, axis=tuple([i for i in range(1, len(load.shape))]))
    return np.append(memory, load*128)    

memory = np.array([], dtype='int32')
#np.load("weights_c0.npy")
memory = load_memory_zp("weights_c0.npy", memory)
memory = load_memory_zp("weights_c1.npy", memory)
memory = load_memory_zp("weights_c2.npy", memory)
memory = load_memory_zp("weights_f.npy", memory)

mem = np.array([convert_to_multiple_bits_32(i) for i in memory], dtype='uint8')

with open('.\\zp\\zeropoint.mif', 'w') as f:
    mif.dump(mem, f, address_radix='UNS', data_radix='UNS')
    