# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 04:02:31 2023

@author: Gabriel
"""

import math
from functools import reduce
import numpy as np

#test_scale = 0.1 
#test_zq = 10
test_scale = 0.18357668817043304 
test_zq = -39

aq_range = 2**16

def range():
    max_value = (127-test_zq)*test_scale
    min_value = -(128-test_zq)*test_scale
    print("Max Value:" + str((127-test_zq)*test_scale))
    print("Max Value:" + str((-128-test_zq)*test_scale))
    return max_value - min_value

range()

new_scale = test_scale / 4 # Como TF faz para 8-bits e queremos 10-bit

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def softmax(x):
  return math.exp(x)

def to_bin(x):
    if(x < 0):
        return f'{65535+x+1:016b}'
    return f'{x:016b}'

def calculate(x):
    calculation = softmax(x * scale)
    return round(calculation / activation_scale)

def quant(x):
    return test_scale * (x - test_zq)

def calc_scale(max_value, min_value, bits):
    return (max_value - min_value) / (2**bits)
    #pois um ponto a mais(1025 pontos)

def calc_zeropt(min_value, scale):
    return 2**9+round(min_value/scale)

max_range = 2 ** 9 # 9 bits pois int10
max_value = quant(127)
scale = max_value / max_range

activation_scale = softmax(max_value) / (2 ** 26) # 26 bits pois int27
