# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 04:02:31 2023

@author: Gabriel
"""
import mif
import math
from functools import reduce
import numpy as np

#test_scale = 0.1 
#test_zq = 10
test_scale = 0.18357668817043304 
test_zq = -39

aq_range = 2**16

def testrange():
    max_value = (127+test_zq)*test_scale
    min_value = -(128+test_zq)*test_scale
    print("Min Value:" + str((-128+test_zq)*test_scale))
    print("Max Value:" + str((127+test_zq)*test_scale))
    return max_value - min_value

testrange()

new_scale = test_scale / 4 # Como TF faz para 8-bits e queremos 10-bit
new_zq = test_zq * 4

print(str((511+new_zq)*new_scale))
print(str((-512+new_zq)*new_scale))

print(str((50+test_zq)*test_scale))
print(str((50*8+new_zq)*new_scale))

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def softmax(x):
  return math.exp(x)

def to_bin(x):
    if(x < 0):
        return f'{65536+x:016b}'
    return f'{x:016b}'

def calculate(x):
    calculation = sigmoid((x+new_zq) * new_scale)
    calculation = round(calculation / activation_scale)
    return calculation if calculation < 32768 else 32767
    

max_range = 2 ** 9 # 10 bits pois int10
#max_value = quant(127)
#scale = max_value / max_range

activation_scale = 1 / (2 ** 15) # 15 bits pois int16

activation_list = [calculate(i) for i in range(0, max_range)] + [calculate(i) for i in range(-max_range, 0, 1)]

with open('test.mif') as f:
    mem = mif.load(f)
    
for i in range(0, 1024):
    binary = [*(to_bin(activation_list[i])[::-1])]
    binary = [int(i) for i in binary]
    mem[i] = np.array(binary, dtype='uint8')

with open('sigmoid.mif', 'w') as f:
    mif.dump(mem, f, address_radix='UNS', data_radix='UNS')

print(calculate(512))