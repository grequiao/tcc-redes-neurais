# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 01:56:36 2023

@author: Gabriel
"""

def fix_line(x, i):
    if(len(x) < 16):
        return x
    fix = f'{i*8:0>4X}'
    return x[:3] + fix + x[7:]
    

with open('sigmoidhex.hex', 'r+') as file:
    # Assume que % 16 == 0
    lines= file.readlines()
    fixed = [fix_line(lines[i], i) for i in range(0, len(lines))]
    file.seek(0, 0)
    file.writelines(fixed)