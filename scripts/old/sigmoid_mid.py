# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 02:28:26 2023

@author: Gabriel
"""

#f'{16:016b}'

import mif

import math
from functools import reduce
import numpy as np

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def to_bin(x):
    if(x < 0):
        return f'{65535+x+1:016b}'
    return f'{x:016b}'

def cal_pos(x):
    calculation = sigmoid(x * scale)
    return round(calculation / activation_scale)

def cal_neg(x):
    calculation = sigmoid(-(x+1) * scale)
    return round(calculation / activation_scale)

max_range = 2 ** 9 # 9 bits pois int10
scale = 4 / max_range

activation_scale = 1 / (2 ** 15) # 15 bits pois int16

#activation_list = [cal_pos(i) for i in range(0, max_range)] + [cal_neg(i) for i in range(0, max_range)]
activation_list = [cal_pos(i) for i in range(0, max_range)] + [cal_neg(i) for i in range(max_range, 0, -1)]

with open('test.mif') as f:
    mem = mif.load(f)
    
for i in range(0, 1024):
    binary = [*(to_bin(activation_list[i])[::-1])]
    binary = [int(i) for i in binary]
    mem[i] = np.array(binary, dtype='uint8')

with open('sigmoid.mif', 'w') as f:
    mif.dump(mem, f, address_radix='UNS', data_radix='UNS')
    

