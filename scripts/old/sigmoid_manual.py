# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 01:01:37 2023

@author: Gabriel
"""

#f'{15:0>4X}'

#a = list(map(lambda x: f'{x:0>4X}', activation_list))

import math
from functools import reduce

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def cal_pos(x):
    calculation = sigmoid(x * scale)
    return round(calculation / activation_scale)

def cal_neg(x):
    calculation = sigmoid(-(x+1) * scale)
    return round(calculation / activation_scale)

max_range = 2 ** 9 # 9 bits pois int10
scale = 4 / max_range

activation_scale = 1 / (2 ** 15) # 15 bits pois int16

activation_list = [cal_pos(i) for i in range(0, max_range)] + [cal_neg(i) for i in range(0, max_range)]

list_mapped = list(map(lambda x: f'{x:0>4X}', activation_list))
list_mapped = list(map(lambda x: x[2:4] + x[0:2], list_mapped))

with open('sigmoidhex.hex', 'w') as file:
    # Assume que % 16 == 0
    for i in range(0, len(activation_list) // 8):
        two_complement = reduce(lambda x, y: x+y, activation_list[i*8:(i+1)*8])
        two_complement = two_complement % 256
        two_complement = ~two_complement + 1
        two_complement = 256 + two_complement
        str_to_write = f":10{i*8:0>4X}00" + ''.join(list_mapped[i*8:(i+1)*8]) + f'{two_complement:0>4X}\n'
        file.write(str_to_write)
    file.write(":00000001FF")
    