# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 22:50:34 2023

@author: Gabriel
"""

import math
from intelhex import IntelHex

def sigmoid(x):
  return 1 / (1 + math.exp(-x))


ih = IntelHex()

max_range = 2 ** 9 # 9 bits pois int10
scale = 4 / max_range

activation_scale = 1 / (2 ** 15) # 15 bits pois int16


for i in range(0, max_range):
    calculation = sigmoid(i * scale)
    # calcular o valor da sigmoid
    calculation = round(calculation / activation_scale)
    # calcular o valor quantizado
    
    # inserir no arquivo hex
    # formato little endian
    ih[2*i] =  calculation & 0xFF
    ih[2*i+1] =  calculation >> 8
      
    # fazer a mesma coisa mas para o negativo
    calculation = sigmoid(-(i+1) * scale)
    # calcular o valor da sigmoid
    calculation = round(calculation / activation_scale)
    # calcular o valor quantizado
    
    # inserir no arquivo hex
    # formato little endian
    ih[2*(i+max_range)] =  calculation & 0xFF
    ih[2*(i+max_range)+1] =  calculation >> 8

#for i in range(1, max_range+1):
#    ih[2*(i+max_range-1)] =  calculation & 0xFF
#    ih[2*(i+max_range-1)+1] =  calculation >> 8


#with open('sigmoidhex.hex', 'w') as file:
#    ih.write_hex_file(file)


#ih.tofile("sigmoidhex2.hex", format='hex')
ih.write_hex_file("sigmoidhex.hex")