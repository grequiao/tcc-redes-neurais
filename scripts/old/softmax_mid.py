# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 02:28:26 2023

@author: Gabriel
"""

#f'{16:016b}'

import mif

import math
from functools import reduce
import numpy as np

layer_scale = 0.029553622007369995 
layer_zq = 54

def softmax(x):
  return exp(x)

def to_bin(x):
    if(x < 0):
        return f'{65535+x+1:016b}'
    return f'{x:016b}'

def cal_pos(x):
    calculation = softmax(x * scale - )
    return round(calculation / activation_scale)

def cal_neg(x):
    calculation = softmax(-(x+1) * scale)
    return round(calculation / activation_scale)

def quant(x):
    return 0.029553622007369995 * (x + 54)

max_range = 2 ** 9 # 9 bits pois int10
max_value = quant(127)
scale = max_value / max_range

activation_scale = softmax(max_value) / (2 ** 26) # 26 bits pois int27

activation_list = [cal_pos(i) for i in range(0, max_range)] + [cal_neg(i) for i in range(0, max_range)]

with open('test.mif') as f:
    mem = mif.load(f)
    
for i in range(0, 1024):
    binary = [*(to_bin(activation_list[i])[::-1])]
    binary = [int(i) for i in binary]
    mem[i] = np.array(binary, dtype='uint8')

with open('softmax.mif', 'w') as f:
    mif.dump(mem, f, address_radix='UNS', data_radix='UNS')
    

