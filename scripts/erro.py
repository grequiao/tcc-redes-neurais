# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 22:36:24 2023

@author: Gabriel
"""

import matplotlib.pyplot as plt
import math
from functools import reduce
import numpy as np

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def quantize(x, activation_scale):
    return round(x / activation_scale)

def quantized_sigmoid(x, activation_scale):
    return sigmoid(x * activation_scale)

scale = 1 / 2**16

min_value = -2**16 * 5
max_value = 2**16 * 5

bits = 4

#x = [i * scale for i in range(min_value, max_value)]
#y = [sigmoid(i) for i in x]
#plot(x,y)

activation_scale = 1 / 2**bits

x = [(i * scale) for i in range(0, 2**18)]
y = [sigmoid(i) - quantized_sigmoid(quantize(i, activation_scale), activation_scale) for i in x]
plt.plot(x,y)
plt.show()

activation_scale = 1 / 2**(bits*2)

y = [sigmoid(i) - quantized_sigmoid(quantize(i, activation_scale), activation_scale) for i in x]
plt.plot(x,y)
plt.show()

activation_scale = 1 / 2**bits

def interpolated_sigmoid(x, activation_scale):
    q_x = math.floor(x / activation_scale)
    alpha = x / activation_scale - q_x
    interpolated = (1 - alpha) * quantized_sigmoid(q_x, activation_scale) + alpha * quantized_sigmoid(q_x+1, activation_scale)
    return interpolated

y = [sigmoid(i) - interpolated_sigmoid(i, activation_scale) for i in x]
plt.plot(x,y)


plt.show()