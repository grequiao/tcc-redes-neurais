# -*- coding: utf-8 -*-
"""
Created on Sat Nov 25 23:59:52 2023

@author: Gabriel
"""
#f'{16:016b}'

import mif
import math
from functools import reduce
import numpy as np

#test_scale = 0.1 
#test_zq = 10
test_scale = 0.18357668817043304 
test_zq = -39


#test_scale = 1 / (2**5-1)
#test_zq = 0

aq_range = 2**8

def testrange():
    max_value = (127+test_zq)*test_scale
    min_value = -(128+test_zq)*test_scale
    print("Min Value:" + str((-128+test_zq)*test_scale))
    print("Max Value:" + str((127+test_zq)*test_scale))
    return max_value - min_value

testrange()

new_scale = test_scale / 4 # Como TF faz para 8-bits e queremos 10-bit
new_zq = test_zq * 4


def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def to_bin(x):
    if(x < 0):
        return f'{255+x+1:08b}'
    return f'{x:08b}'

def calculate(x):
    calculation = sigmoid((x+new_zq) * new_scale)
    calculation = round(calculation / activation_scale)
    if calculation > 127:
        print(x)
        print(calculation)
    return calculation if calculation < 127 else 127

max_range = 2 ** 9 # 10 bits pois int10
#max_value = quant(127)
#scale = max_value / max_range

activation_scale = 1 / (2 ** 7) # 15 bits pois int16

activation_list = [calculate(i) for i in range(0, max_range)] + [calculate(i) for i in range(-max_range, 0, 1)]

with open('base_8bit.mif') as f:
    mem = mif.load(f)
    
for i in range(0, 1024):
    binary = [*(to_bin(activation_list[i])[::-1])]
    binary = [int(i) for i in binary]
    mem[i] = np.array(binary, dtype='uint8')

with open('sigmoid_8bit.mif', 'w') as f:
    mif.dump(mem, f, address_radix='UNS', data_radix='UNS')
    
print(calculate(512))
