# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 03:17:39 2023

@author: Gabriel
"""
from matplotlib.pyplot import plot
import math
from functools import reduce
import numpy as np

def array_to_64bit(x):
    assert x.shape[0] % 8 == 0
    
    cover = x.shape[0] / 8
    list_array = np.split(x, cover)
    list_array = [to_64bit(i) for i in list_array]
    return list_array

def to_64bit(x):
    #converter para int64
    typed = x.astype('int64')
    
    number = typed[0]
    number += typed[1] << 8
    number += typed[2] << 16
    number += typed[3] << 24
    number += typed[4] << 32
    number += typed[5] << 40
    number += typed[6] << 48
    number += typed[7] << 56
    return number

weights_c0 = np.load("weights_c0.npy")
#divided = np.split(weights_c0, 8)
divided = [weights_c0[i::8] for i in range(0, 8)]

#np.pad(c, [(0, 0), (0, 0), (0,0), (0,7)], mode='constant', constant_values=0).shape
pad_func = lambda x: np.pad(x, [(0, 0), (0, 0), (0,0), (0,7)], mode='constant', constant_values=0)

divided = list(map(pad_func, divided))

flat_map = []

for i in divided[0]:
    for jar in i:
        for k in jar:
            flat_map = np.append(flat_map, array_to_64bit(k))
            
weights_f = np.load("weights_f.npy")